# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.6.0

- minor: Add multi-build platform

## 0.5.0

- minor: Add multi-build platform

## 0.4.0

- minor: Update pipe's base docker image to python:3.11-alpine.

## 0.3.0

- minor: Refactor pipe.

## 0.2.0

- minor: Update pipe's base docker image to python:3.11-alpine.

## 0.1.4

- patch: Add multi-build

## 0.1.3

- patch: Refactor.

## 0.1.2

- patch: Refactor.

## 0.1.1

- patch: Update pipelines.

## 0.1.0

- minor: Initial release
